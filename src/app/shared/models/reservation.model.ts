export interface Pricing {
  name: string;
  position: number;
  price: number;
  priceMore: number;
  duration: number;
}
